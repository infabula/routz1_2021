import sys


def load_confuration():
    print("Loading configuration")

def save_configuration():
    print("Saving confuration")

def quit():
    print("Quiting")

def what_to_do():
    do = input("What to do? [load, save, quit]")
    if do.lower() == 'load':
        load_confuration()
    elif do.lower() == 'save':
        save_configuration()
    elif do.lower() == 'quit':
        quit()
    else:
        print("Unknown action")


def main():
    what_to_do()


if __name__ == '__main__':
    main()
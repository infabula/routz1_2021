def ask_amount():
    amount = int(input("Hoeveel?\n"))
    if amount < 0:
        print("Mag niet negatief")
        return 0
    else:
        return amount


def main():
    amount = ask_amount()
    message = "The user wants " + str(amount)
    print(message)


main()
def main():
    price_per_month = input("What is the price per month for one VPS? ")
    price_per_month = float(price_per_month)
    vps_count = int(input("How many VPS servers do you need? "))
    provision_minutes = int(input("How many minutes needs an administrator to provision one VPS? "))

    admin_hour_rate = 84.0

    vps_price = price_per_month * vps_count
    provision_price = vps_count * provision_minutes * (admin_hour_rate / 60.0)

    total = vps_price + provision_price
    rounded_total = round(total, 2)

    print("The total cost for", vps_count, "servers is", total, "euro.")


main()
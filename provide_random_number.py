from random import randint


def ask_min_max_numbers():
    ''' vraag de gebruiker om een minimale waarde
    vraag daarna in een oneindige loop een maximale waarde
    blijf in de loop als de maximale waarde kleiner is dan de minimale waarde
    dat is namelijk niet logisch
    break uit de loop als de maximale waarde groter is dan de minimale waarde
    en retourneer beide waarden
    '''
    minimum = int(input("Minimum waarde:"))
    while True:
        maximum = int(input("Maximum waarde:"))
        if maximum > minimum:
            return minimum, maximum
        print("Maximum moet groter zijn dan ", minimum)



def select_random_number(minimum, maximum):
    '''Start een oneindige loop waarin je een random getal genereert tussen min en max.
    Toon het nummer en vraag of dit getal geselecteerd moet worden
    Indien de gebruiker 'yes' ingeeft, retourneer je het getal.
    In alle andere gevallen zeg je dat je een een nieuw getal gaat maken en je blijft in de loop.
    '''
    while True:
        number = randint(minimum, maximum)
        user_input = input("Wil je " + str(number) + "?")
        if (user_input.lower() == 'yes') or (user_input.lower() == 'ja'):
            return number


def main():
    minimum, maximum = ask_min_max_numbers()
    number = select_random_number(minimum, maximum)

    message = f'Ok, je hebt gekozen voor {number} dat tussen {minimum} en {maximum} ligt.'
    print(message)


if __name__ == '__main__':
    main()
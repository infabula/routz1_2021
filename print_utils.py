print("Print utils", __name__)
value = 42


def print_stars_line_length_ten():
    line = '*' * 10
    print(line)

def print_stars_line(length):
    stars = "*" * int(length)
    print(stars)

def print_line(length, character="*"):
    line = str(character) * int(length)
    print(line)


if __name__ == "__main__":
    print("Kom laten we wat print lines gaan testen")
    print_line(30, '$')
def ask_user_hostname():
    name = input("Hostname:")
    name = name.strip()
    if not name:
        raise ValueError("Geen hostname gegeven.")
    else:
        return name


def ask_user_ip():
    ip = input("IP:")
    return ip.strip()


def print_dns(dns):
    """Print de dns data"""
    print("De dns data:")
    for name, ip in dns.items():
        print(name, ip)


def add_to_dns(dns, hostname, ip):
    """Voeg to aan dns"""
    if not hostname in dns:
        dns[hostname] = ip  # add
    else:
        existing_ip = dns[hostname]
        print(f"{hostname} zit in in de dns met ip {existing_ip}")
        add = input("Overschrijven?")
        if add.lower() == 'ja':
            dns[hostname] = ip


def user_interact(dns):
    while True:
        try:
            print_dns(dns)
            hostname = ask_user_hostname()
            ip = ask_user_ip()
            add_to_dns(dns, hostname, ip)
        except ValueError:
            print("goed, we stoppen")
            break


def main():
    dns = { 'rjekker.nl': '136.144.169.117',
            'google.com': '172.217.20.110',
            'localhost': '127.0.0.1'
    }

    user_interact(dns)


if __name__ == "__main__":
    main()
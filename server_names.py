import os.path


def ask_servernames(filename):
    outfile = open(filename, 'w')
    while True:
        user_input = input("Server name: ")
        server_name = user_input.strip()
        if not server_name:   # truefy falsey!
            break
        outfile.write(server_name + '\n')
    outfile.close()


def print_names_from_file(filename):
   with open(filename, 'r') as infile:
       for line in infile:
           name = line.strip()
           print('-', name)


def main():
    try:
        filename = os.path.join('.', "servers.txt")
        ask_servernames(filename)
        print_names_from_file(filename)
    except Exception as e:
        print("Something went wrong...")
        print(e)


if __name__ == "__main__":
    main()

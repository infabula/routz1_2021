import print_utils as pu

'''
from print_utils import print_stars_line_length_ten
from print_utils import print_stars_line
from print_utils import print_line
from print_utils import value
'''


value = "tweeenveertig"

print("Print lines file", __name__)

def main():
    line_length = 25

    pu.print_stars_line_length_ten()
    pu.print_stars_line(line_length)
    pu.print_stars_line(line_length + 5)

    char = '#'
    pu.print_line(line_length, character="%")
    pu.print_line(length=line_length, character=char)


if __name__ == "__main__":
    main()
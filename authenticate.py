import sys


def ask_username():
    username = input("username:")
    username = username.strip()
    if len(username) < 3:
        sys.exit(1)
    elif ' ' in username or '\t' in username:
        sys.exit(42)

    return username


def ask_password(username):
    pwd = input("password: ")
    if len(pwd) < 5:
        sys.exit(43)
    if pwd == username:
        sys.exit(5556)
    return pwd


def is_welcome(name, passwd):
    if name == 'Bob' and passwd == 'secret':
        return True
    elif (name == 'Alice' or name == 'Eve') and passwd == '124345':
        return True
    return False


def main():
    username = ask_username()
    password = ask_password(username)

    if is_welcome(username, password):
        message = "Welcome in " + username + "."
    else:
        message = "Not allowed."
        print(message)
    sys.exit(0)


if __name__ == "__main__":
    main()
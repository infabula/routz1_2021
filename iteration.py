for count in range(0, 51, 5):
    if count > 30:
        break
    elif count == 15:
        continue
    print(count)


def count_with_while():
    count = 0
    while count <= 100:
        print(count)
        count += 1

class MyGroovyMistake(ValueError):
    pass


class OctetError(ValueError):
    pass


def ask_number(minimum=0, max_tries=3):
    for count in range(3):
        num = int(input(f"Een getal groter gelijk {minimum}"))
        if num >= minimum:
            return num
        print(f"Groter dan {minimum} graag")

    raise MyGroovyMistake("Gebruiker kan geen keuze maken")

def generate_ip_addresses(start_octet, end_octet, base="192.168.178"):
    """
    Generates ip addresses from start_octet until and include end_octet
    :param start_octet:
    :param end_octet:
    :param base:
    :return: None
    """
    if start_octet < 0 or end_octet > 255:
        raise OctetError("Octet is out or range")

    for octet in range(start_octet, end_octet+1):
        ip = f"{base}.{octet}"
        print(ip)


def main():
    print("Hoeveel ip-adressen moeten er worden gemaakt?")

    try:
        ip_count = ask_number()

        print("Wat is het start-octet?")
        ip_start = ask_number(minimum=1, max_tries=2)
        ip_end = ip_start + ip_count - 1

        generate_ip_addresses(ip_start, ip_end)

    except OctetError as e:
        print("Octet Error opgetreden:")
        print(e)
    except MyGroovyMistake as e:
        print("Een groovy ongeldige waarde.")
        print(e)
    except ValueError as e:
        print("Een ongeldige waarde.")
        print(e)
    except Exception as e:
        print("Tijd om te loggen:")
        print(e)
    finally:
        print("Finale code")


if __name__ == "__main__":
    main()
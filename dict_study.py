def main():

    data = {
        'interface':   {'name': 'Loopback_987453',
                        'description': 'A loopback device',
                        'type': 'iana-if-type:softwareLoopback',
                        'enabled': True,
                        'ipv4': {'address': [{'ip': '10.0.0.5',
                                              'netmask': '255.255.255.0'}
                                             ]
                                 },
                        'ipv6': {}
                       }
    }

    # print alle keys van de interface
    for key in data['interface']:
        print(key)

    # print de naam van de interface
    print(data['interface']['name']

    # wijzig naam
    data['interface']['name'] = "LoopEenEindje"

    # wijzig de waarde 'enabled' naar False
    data['interface']['enabled'] = False

    # print het ip-adres van ipv4


import os, os.path


def open_in_context():
    with open('bericht.txt') as infile:
        content = infile.read()

    # file is al weer afgesloten
    print(content)


def write_to_file():
    outfile = open('bericht.txt', 'w')
    outfile.write("Dag wereld\nWat is het weer toch koud.")
    outfile.close()

def file_paths():
    # a relative path
    relative_dir = os.path.join('some_dir')

    # absolute path
    # use os.sep: this is a slash or backslash
    homedir = os.path.join(os.sep, 'home', 'reindert')

    print(relative_dir)
    print(homedir)

def main():
    #write_to_file()
    #open_in_context()
    file_paths()


main()
def set_age(age):
    if age < 0:
        raise ValueError("Age can't be negative")

try:
    age = int(input("age:"))
    set_age(age)

except ValueError as e:
    print("Could not set the age")
    # print(e)

except Exception as e:
    print("Something else went wrong.")
    print(e)


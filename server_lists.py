def add_some(servers):
    servers.append("Helsinki")
    paris_index = servers.index('Paris')
    servers.insert(paris_index, "Madrid")

def get_five_letter_servers(servers):
    five_letter_names = []
    for name in servers:
        if len(name) == 5:
            five_letter_names.append(name)
    return five_letter_names


def print_reverse(a_list):
    copy = a_list[:]
    while copy:
        name = copy.pop()
        print(name)

def main():
    servers = ['Amsterdam', 'Tokyo', 'Paris', 'Brussel']
    add_some(servers)
    message = "Now there are " + str(len(servers)) + " servers."
    print(message)
    filtered = get_five_letter_servers(servers)
    print("There are", len(filtered), "names with 5 characters")
    print_reverse(filtered)


if __name__ == "__main__":
    main()

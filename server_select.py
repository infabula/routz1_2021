def print_indexed_names(servers):
    '''print de namen van de servers met de list-index ervoor'''
    """
    for index in range(len(servers)):
        server = servers[index]
        server_name = server[0]
        print(f"{index:4d} {server_name}")
    """

    for index, server in enumerate(servers):
        server_name = server[0]
        print(f"{index:4d} {server_name}")


def ask_index(max_index):
    while True:
        try:
            index = int(input("Van welke server wil je de details zien?"))
            if (index >= 0) and (index <= max_index):
                return index
            else:
                print("Dat is een ongeldige index.")
        except ValueError:
            print("Een getal graag.")


def print_properties(index, servers):
    '''Print de properties van de server'''
    try:
        server = servers[index]
        name = server[0]
        ip = server[1]
        netmask = server[2]
        print(f"Server name: {name}")
    except Exception:
        print("Index ligt buiten de lijst.")


def main():
    servers = [ ["Amsterdam", '192.168.178.3', '255.255.255.0'],
                ["Tokyo", '192.168.178.8', '255.255.255.0'],
                ["Paris",  '192.168.178.12', '255.255.255.0'],
                ["Helsinki",  '192.168.178.145', '255.255.255.0'],
                ["Brussel",  '192.168.178.12', '255.255.255.0']]

    print_indexed_names(servers)
    max_index = len(servers)
    index = ask_index(max_index)
    print_properties(index, servers)


if __name__ == "__main__":
    main()